// Default JavaScript Functions and Initiations
$(document).ready(function() {
	var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var controls;
// GUI
var FizzyText = function() {
  this.message = 'elements';
  this.Xposition = 2;
  this.Yposition = 2;
    this.Zposition = 0;
  // Define render logic ...
};


  var text = new FizzyText();
  var gui = new dat.GUI();
  gui.add(text, 'message');
  gui.add(text, 'Xposition', -10, 10);
  gui.add(text, 'Yposition', -10, 10);
    gui.add(text, 'Zposition', -10, 10);

// 3D
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 15 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

controls = new THREE.OrbitControls( camera );
controls.addEventListener( 'change', render );

// General vars
var activeBoxes = [];
var availableBoxes = [];
var chance = 25;

var material = new THREE.MeshDepthMaterial({wireframe:false,wireframeLinewidth:1})//MeshLambertMaterial({color:clr})//MeshBasicMaterial( { color: clr } );
	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	var cube = new THREE.Mesh( geometry, material );
	var cube2 = new THREE.Mesh( new THREE.BoxGeometry( 0.5, 0.5, 0.5 ), material );
	var cube3 = new THREE.Mesh( new THREE.BoxGeometry( 0.5, 0.5, 0.5 ), material );
	cube3.position.x = cube2.position.x = 2;

	scene.add( cube, cube2, cube3 );
	
	var geometry2 = new THREE.SphereGeometry( 0.1, 0.1, 0.1 );
var sphere = new THREE.Mesh( geometry2, new THREE.MeshBasicMaterial({color:0xff0000}));
scene.add(sphere);

var material = new THREE.LineBasicMaterial({
	color: 0x0000ff
});
availableBoxes.push(cube2);
/*var geometry = new THREE.Geometry();
geometry.vertices.push(
	new THREE.Vector3( -10, 0, 0 ),
	new THREE.Vector3( 0, 10, 0 ),
	new THREE.Vector3( 10, 0, 0 )
);

var line = new THREE.Line( geometry, material );
scene.add( line );*/
// LIGHT
/*var light = new THREE.PointLight( 0xff0000, 1, 100 );
light.position.set( 5, 5, 5 );
scene.add( light );*/


//scene.fog = new THREE.Fog( 0x000000, 1,10 )

// CREATE ATTACKING BOXES
/*for (var i = 24,y = 5,x = 5; i >= 0; i--) {

	var remainder = i%5;
	y = (remainder==4)? y-1-0.1:y;
	x = (remainder==4)? 4:x-1-0.1;

var clr = (i%2)? 0xff0000:0xff0000;
	//console.log("cube "+i+"> "+(x)+" - "+ (y)+" - "+0+"clr"+clr );
	
	var material = new THREE.MeshDepthMaterial({wireframe:false,wireframeLinewidth:5})//MeshLambertMaterial({color:clr})//MeshBasicMaterial( { color: clr } );
	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	var cube = new THREE.Mesh( geometry, material );

	cube.position.set(x-2, y-2, -10);
	scene.add( cube );
	availableBoxes.push(cube);

};*/

// LOADING EXTERNAL ASSET
// instantiate a loader
/*var loader = new THREE.OBJLoader();

var spaceship;
// load a resource
loader.load(
	// resource URL
	'source/3d/spaceship2.obj',
	// Function when resource is loaded
	function ( object ) {
		var sGeometry = new THREE.Geometry().fromBufferGeometry( object.children[0].geometry );

		var m = new THREE.MeshDepthMaterial({wireframe:false,wireframeLinewidth:5});
		//spaceship.material = m;
		spaceship = new THREE.Mesh(sGeometry,m);
		scene.add( spaceship );
		spaceship.scale.set(0.3,0.3,0.3)
		spaceship.rotation.y = 270*Math.PI/180
		//spaceship.rotateOnAxis(new THREE.Vector3(0,1,0), 90*Math.PI/180)
		//spaceship.children[0].rotateOnAxis(new THREE.Vector3(0,1,0), 90*Math.PI/180)
	//	m.{wireframe:true,wireframeLinewidth:10
		//m.wireframe = true;
		//m.wireframeLinewidth=10;
		//console.log(m.wireframeLinewidth);
		//spaceship.children[0].material = m;
		//spaceship.children[0].rotateOnAxis(new THREE.Vector3(0,1,0), 90*Math.PI/180)
		console.log(object);
console.log(availableBoxes[0]);
	}
);*/


// REPOSITION CAMERA
camera.position.z = 5;
var aObjects = [cube];
var rays = [
     new THREE.Vector3(1, 0, 0),new THREE.Vector3(-1, 0, 0),new THREE.Vector3(0, 1, 0),new THREE.Vector3(0, -1, 0)
    ];
var pY = 0;
function render() {
	requestAnimationFrame( render );
	//cube.position.x = text.Xposition;
	//cube.position.y = text.Yposition;
	//cube.position.z = text.Zposition;
	renderer.render( scene, camera );
	
	/*for ( var key in scene.children ) {
		console.log(key);
		//key.position.z = text.Zposition;
	}*/

cube2.position.x = text.Xposition;
cube2.position.y = text.Yposition;
cube2.position.z = text.Zposition;
//cube.material.wireframe=false;
// calculate objects intersecting the picking ray
/* for (n = 0; n < rays.length; n += 1) {
raycaster.set( cube2.position, rays[n] );
	var intersects = raycaster.intersectObjects( aObjects );



	for (var r = aObjects.length - 1; r >= 0; r--) {

for ( var i = 0; i < intersects.length; i++ ) {
			

		if(aObjects[r].uuid==intersects[i].object.uuid)
		{
		intersects[i].object.material.wireframe=true;console.log("intersects");
		console.log(intersects[i].distance)
	}
		
	};
	}
}*/

 var originPoint = cube.position.clone();

        	//console.log(cube2.geometry.vertices.length);

        for (var vertexIndex = 0; vertexIndex < cube.geometry.vertices.length; vertexIndex++)
        {
            var localVertex = cube.geometry.vertices[vertexIndex].clone();

            var globalVertex = localVertex.applyMatrix4( cube.matrix );

            var directionVector = globalVertex.sub( cube.position );
//if(pY==0.01)console.log(directionVector);
            var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );

            var collisionResults = ray.intersectObjects( availableBoxes );
           // console.log(collisionResults);

            if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() )
            {
                console.log(collisionResults);
                cube2.material.wireframe=true;
            }


        }
 //sphere.position.setY(cube.position.y)
 //pY+=0.01;
 //sphere.position.y = cube2.position.y;
 //if(pY==0.02)console.log(originPoint);
  //if(pY==0.02)console.log(sphere.position);

        /*
var material = new THREE.LineBasicMaterial({ color: 0x0000ff }); var geometry = new THREE.Geometry(); geometry.vertices.push( new THREE.Vector3( -10, 0, 0 ), new THREE.Vector3( 0, 10, 0 ), new THREE.Vector3( 10, 0, 0 ) ); var line = new THREE.Line( geometry, material ); scene.add( line );
        */
	// SELECT CUBE TO USE
	/*var removeCube = Math.floor(Math.random()*chance);

	// REMOVE IT FROM AVAILABLE CUBES LIST AND ADD IT TO USED CUBES LIST
	if(removeCube<=availableBoxes.length-1) {
		
		var tempC = availableBoxes.splice(removeCube, 1);
		activeBoxes.push(tempC[0]);
		//console.log(tempC[0]);
	}

	// MOVE SELECTTED CUBES
	for (var key in activeBoxes) {
		var tempC = activeBoxes[key];
		tempC.position.z += 0.2;
		
		// REMOVE FROM ACTIVE AND ADD TO AVAILABLE IF ITS BEHIND CAMERA
		if(tempC.position.z > camera.position.z) 
		{
			activeBoxes.splice(key, 1);
			availableBoxes.push(tempC);
			tempC.position.z = -10;
		}



	}
if(spaceship!=undefined)
	{
		//var cube = scene.getObjectByName('cube');
        var originPoint = spaceship.position.clone();

        for (var vertexIndex = 0; vertexIndex < spaceship.geometry.vertices.length; vertexIndex++)
        {
            var localVertex = spaceship.geometry.vertices[vertexIndex].clone();
            var globalVertex = localVertex.applyMatrix4( spaceship.matrix );
            var directionVector = globalVertex.sub( spaceship.position );
            var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );
            var collisionResults = ray.intersectObjects( availableBoxes );
            if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() )
            {
                console.log("COLLISION");
            }
        }*/

		//spaceship.position.setX(text.Xposition);
		//spaceship.position.setY(text.Yposition);
		//spaceship.rotation.y =  text.Zposition*Math.PI/180
	/*	var originPoint = spaceship.position.clone();

for (var vertexIndex = 0; vertexIndex < spaceship.geometry.vertices.length; vertexIndex++)
    {
    	console.log("check");
        var localVertex = spaceship.geometry.vertices[vertexIndex].clone();
        var globalVertex = localVertex.applyMatrix4(spaceship.matrix);
        var directionVector = globalVertex.sub(spaceship.position);

        var ray = new THREE.Raycaster(spaceship.position, directionVector.clone().normalize());
        var collisionResults = ray.intersectObjects(availableBoxes);
        
        if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length())
        {
            console.log("COLLISION");
        }
    }
	}*/

//THREE.Matrix4: .multiplyVector3() has been removed. Use vector.applyMatrix4( matrix ) or vector.applyProjection( matrix ) instead.

// DETECTING COLLISIONS
/*for (var vertexIndex = 0; vertexIndex < spaceship.geometry.vertices.length; vertexIndex++)
{       
    var localVertex = spaceship.geometry.vertices[vertexIndex].clone();
    var globalVertex = localVertex.applyMatrix4(spaceship.matrix)// spaceship.matrix.multiplyVector3(localVertex);   
    var directionVector = globalVertex.subSelf( spaceship.position );

    var ray = new THREE.Ray( spaceship.position, directionVector.clone().normalize() );
    var collisionResults = ray.intersectObjects( availableBoxes );
    if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() ) 
    {
        // a collision occurred... do something...
        console.log("COLLISION");
    }
}*/

// ON INIT CHANCE IS LOW SO THAT A BOX STARTS FLYING IT INCREASES TO BECOME MORE RANDOM
//chance = 100;
}
render();
controls.update();

}) // end document ready
