// Default JavaScript Functions and Initiations
$(document).ready(function() {

        // SETTING UP THE GUI
        var HeadGameGUI = function() {
            this.message = 'Controls';
            this.sickMode = false;
            this.enableTimer = false;
            this.Xposition = 0;
            this.Yposition = 0;
            this.Zposition = 0;
            this.Xrotation = 0;
            this.Yrotation = 0;
            this.Zrotation = 0;

        };


        var hgg = new HeadGameGUI();
        var gui = new dat.GUI();
        gui.add(hgg, 'message');
        gui.add(hgg, 'sickMode');
        gui.add(hgg, 'enableTimer');
        var xController = gui.add(hgg, 'Xposition', -10, 10);
        gui.add(hgg, 'Yposition', -10, 10);
        gui.add(hgg, 'Zposition', -10, 10);
        gui.add(hgg, 'Xrotation', 0, 360);
        gui.add(hgg, 'Yrotation', 0, 360);
        gui.add(hgg, 'Zrotation', 0, 360);

        // EXTENDING MESH MATERIAL
        THREE.MeshDepthMaterial.prototype.dissolving = false;
        THREE.MeshDepthMaterial.prototype.dissolveMe = function() {
            //console.log("dissolve me");
            this.wireframe = true;
            this.dissolving = true;
            var dissolveCounter = new MiniDaemon(this, dissolve, 50, 4);
            dissolveCounter.start();

            function dissolve() {
                var newOpacity = this.opacity - 0.25;
                this.opacity = parseFloat(newOpacity.toFixed(2));
            }
        };

        // ADDITIONAL MATERIALS
        var startingMaterial = new THREE.MeshBasicMaterial({
            wireframe: true,
            wireframeLinewidth: 1,
            transparent: false,
            color: 0x00ff00
        })

        //
        //  GAME VARIABLES
        //

        // NOT PLAYING 0, PLAYING 1, END SCREEN 2
        var gameScene = 0;

        // CAMERA STATUS
        var cameraStatus = "no camera";

        // WINDOW SIZE
        var width = window.innerWidth;
        var height = window.innerHeight;

        var speed = 0.05;

        // ROTATION FOR STARTING SCREEN
        var xRotVal = 0;
        var yRotVal = 0;
        var zRotVal = 0;

        // LIVES CONTAINER
        var lives = {
            "container" : document.getElementById("lives-and-score"),
            "total" : 3,
            "images" : [],
            "available" : this.total, 
        }

        // TIMER
        var timer = new THREE.Clock(true);
        timer.start();

        // LAPSED BOXES
        var lapsedBoxes = 0;

        // WHEN TO INCREASE SPEED
        var speedTrigger = 30;
        var timeTrigger = 11;
        var increaseSpeed = false;

        // 3D
        var scene = new THREE.Scene();
        var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 15);


        var renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);

        // General vars
        var activeBoxes = [];
        var availableBoxes = [];
        var chance = 25;
        var boxMaterial = new THREE.MeshDepthMaterial({
                wireframe: false,
                wireframeLinewidth: 5,
                transparent: true
            })

        // CREATE ATTACKING BOXES
        for (var i = 24, y = 5, x = 5; i >= 0; i--) {

            var remainder = i % 5;
            y = (remainder == 4) ? y - 1 - 0.1 : y;
            x = (remainder == 4) ? 4 : x - 1 - 0.1;

            var clr = (i % 2) ? 0xff0000 : 0xff0000;
            var geometry = new THREE.BoxGeometry(1, 1, 1);
            var cube = new THREE.Mesh(geometry, boxMaterial);

            cube.position.set(x - 2, y - 2, -10);
            scene.add(cube);
            availableBoxes.push(cube);

        };

        // LOADING EXTERNAL ASSET
        // instantiate a loader
        var loader = new THREE.OBJLoader();

        var spaceship;
        // load a resource
        loader.load(
            // resource URL
            'source/3d/spaceship2.obj',
            // Function when resource is loaded
            function(object) {
                var sGeometry = new THREE.Geometry().fromBufferGeometry(object.children[0].geometry);

                var m = new THREE.MeshDepthMaterial({
                    wireframe: false,
                    wireframeLinewidth: 5,
                    transparent: true
                });
                //spaceship.material = m;
                spaceship = new THREE.Mesh(sGeometry, m);
                scene.add(spaceship);
                spaceship.scale.set(0.3, 0.3, 0.3)
                spaceship.position.setZ(3);
                spaceship.rotation.y = 270 * Math.PI / 180;
                    //console.log(object);
                    //console.log(availableBoxes[0]);
            }
        );

        // REPOSITION CAMERA
        camera.position.z = 5;

        // GET CANVAS ELEMENT GENERATED BY THREE
        var c = renderer.domElement;
        //console.log(c);

        // SHAKE TYPE
        var shakeType = "shake shake-constant shake-hard";
        var startingTime = 0;

        // ADD LIVES
        for (var i = lives.total - 1; i >= 0; i--) {
            
            var spaceshipImage = lives.images[i] = new Image();
            spaceshipImage.src = '/img/spaceship_icon.png';
            spaceshipImage.classList.add('lives');
            lives.container.appendChild(spaceshipImage);

        };

        // SETING UP TWEEN VARIABLES
        var tweening = false;
        var startingLocation = {
            x: 0,
            y: 0,
            z: 0,
            rotX:0,
            rotY:0,
            rotZ:0
        };
        var finalLocation = {
            x: 0,
            y: 0,
            z: 0,
            rotX:0,
            rotY:0,
            rotZ:0
        };

        var tween = new TWEEN.Tween(startingLocation)
                .to(finalLocation, 1000)
                .easing(TWEEN.Easing.Cubic.Out)
                .onStart(function(){    tweening = true})
                .onComplete(function(){ tweening = false})

        function tweenSpaceship(params) {

            TWEEN.removeAll();
            startingLocation.x = spaceship.position.x;
            startingLocation.y = spaceship.position.y;
            startingLocation.z = spaceship.position.z;
            startingLocation.rotX = spaceship.rotation.x;
            startingLocation.rotY = spaceship.rotation.y;
            startingLocation.rotZ = spaceship.rotation.z;
            
            if(params.x)    finalLocation.x    = params.x;
            if(params.y)    finalLocation.y    = params.y;
            if(params.z)    finalLocation.z    = params.z;
            if(params.rotX) finalLocation.rotX = params.rotX;
            if(params.rotY) finalLocation.rotY = params.rotY;
            if(params.rotZ) finalLocation.rotZ = params.rotZ;

            tween.start();
        };

        function render() {

            requestAnimationFrame(render);
            renderer.render(scene, camera);

            // UPDATE TWEENS
            TWEEN.update();


            if (spaceship != undefined) {
                var originPoint = spaceship.position.clone();


                //  STARTING SCREEN
                if (gameScene == 0) {

 
                    spaceship.material = startingMaterial;
                    
                    if (cameraStatus != "found") {
                        yRotVal = (yRotVal == 361) ? 1 : yRotVal + 1;
                        spaceship.rotation.y = decInRad(yRotVal);
                        spaceship.rotation.x = decInRad(360);
                        spaceship.rotation.z = decInRad(320);
                    }
                    else if(tweening)
                    {

                    spaceship.position.x = startingLocation.x;
                    spaceship.position.y = startingLocation.y;
                    spaceship.position.z = startingLocation.z;
                    spaceship.rotation.x = decInRad(startingLocation.rotX);
                    spaceship.rotation.y = decInRad(startingLocation.rotY);
                    spaceship.rotation.z = decInRad(startingLocation.rotZ);
                }
                else
                {
                   
                }
                    //spaceship.position.x = hgg.Xposition;
                    /*spaceship.position.y = hgg.Yposition;

                    var tween = new TWEEN.Tween(spaceship.position).to({
                        x: hgg.Xposition
                    }, 4000).start();*/


                    //createjs.Tween.get(spaceship).to({position.x:hgg.Xposition}, 1000);
                    
                    //spaceship.rotateOnAxis(new THREE.Vector3(1, 0, 0), decInRad(xRotVal));

                }
                // GAME HAS STARTED && PAUSE GAME IF ISSUES WITH CAMERA
                else if (gameScene == 1 && cameraStatus == "found") {

                    // COLLISION DETECTION IF IN GAME 
                    for (var vertexIndex = 0; vertexIndex < spaceship.geometry.vertices.length; vertexIndex++) {
                        var localVertex = spaceship.geometry.vertices[vertexIndex].clone();
                        var globalVertex = localVertex.applyMatrix4(spaceship.matrix);
                        var directionVector = globalVertex.sub(spaceship.position);
                        var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
                        var collisionResults = ray.intersectObjects(activeBoxes);

                        if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
                            //makeAvailable(index, element)
                            /*
                             // REMOVE A LIFE BY MAKING IT INVISIBLE
                                lives.available -= 1;
                                livesContainer.children[lives.available].classList.add('hide');
                            */
//console.log(collisionResults);
                            for (var i = 0; i < collisionResults.length; i++) {
                                console.log(i);
                                // START DESOLVING
                                if (collisionResults[i].object.material.dissolving == false)
                                    {
                                        hits ++;
                                        //collisionResults[i].object.material.dissolveMe();
                                        collisionResults[i].object.material = startingMaterial;
                                        console.log("HIT");
                                        document.getElementById("start-game").innerHTML = ""+hits;

                                    }

                            }

                            // SHAKE ONLY IF SICK MODE HAS BEEN ENABLED
                            if ($(c).hasClass(shakeType) != true && hgg.sickMode == true) {
                                //console.log("START SHAKING");
                                $(c).addClass(shakeType);
                                window.setTimeout(removeShake, 500);
                            }

                        }
                    }

                    // CUBES MOVEMENT
                    // TIME BASED SPEED REGULATOR
                    var timePassed = Math.round(timer.getElapsedTime()) % timeTrigger;
                    startingTime = (startingTime == 0) ? timer.getElapsedTime() : startingTime;

                    var deltaTime = timer.getElapsedTime() - startingTime;
                    var minutes = Math.round(deltaTime / 60);
                    var seconds = Math.round((minutes * 60) - deltaTime) * -1;
                    var millis = Math.abs(Math.round((Math.round(deltaTime) - deltaTime) * 1000));
                    updateTime(minutes + ":" + seconds + ":" + millis);


                    // EVERY 30 BOXES GET FASTER
                    if (timePassed == timeTrigger - 1 && hgg.enableTimer == true) {
                        if (increaseSpeed) speed += 0.01;
                        increaseSpeed = false;
                    } else if (hgg.enableTimer == false) {
                        speed = 0.1;
                    } else {
                        increaseSpeed = true;
                    }

                    // GENERATE RANDOM NUMBER 
                    var removeCube = Math.floor(Math.random() * chance);

                    // IF THE NUMBER CORRESPONDS TO A CUBE IN THE AVAILABLE BOXES ARRAY, 
                    // REMOVE IT FROM AVAILABLE CUBES LIST AND ADD IT TO THE ACTIVE CUBES LIST
                    if (removeCube <= availableBoxes.length - 1) {

                        var tempC = availableBoxes.splice(removeCube, 1);
                        activeBoxes.push(tempC[0]);
                    }

                    // MOVE SELECTTED CUBES
                    for (var key in activeBoxes) {
                        var tempC = activeBoxes[key];
                        tempC.position.z += speed;

                        // REMOVE FROM ACTIVE AND ADD TO AVAILABLE IF ITS BEHIND CAMERA
                        if (tempC.position.z > camera.position.z) {
                            makeAvailable(key, tempC);
                        }



                    }




                }
                // GAME HAS ENDED
                else if (gameScene == 2) {

                }

            }

            // ON INIT CHANCE IS LOW SO THAT A BOX STARTS FLYING IT INCREASES TO BECOME MORE RANDOM
            chance = 100;

        }
        var hits = 0;


        // START RENDERING
        render();

        // OVERLAY TEXT SCREEN
        var statsDiv = document.getElementById("game-stats");
        onResize(null);

        // REMOVE SHAKE
        function removeShake() {
            $(c).removeClass(shakeType);
            //console.log("stop shaking");
        }

        // REMOVING CUBES FUNCTION
        function makeAvailable(index, element) {
            activeBoxes.splice(index, 1);
            availableBoxes.push(element);
            element.material = boxMaterial;
            element.position.z = -10;
            element.material.wireframe = false;
            element.material.opacity = 1;
           // element.material.dissolving = false;
            lapsedBoxes++;
        }

        // INIT FACE FINDER

        // Grab elements, create settings, etc.
        var canvas = document.getElementById("compare"),
            context = canvas.getContext("2d"),
            video = document.getElementById("video"),
            debugOverlay = document.getElementById('debug'),
            videoObj = {
                "video": true
            },
            errBack = function(error) {
                console.log("Video capture error: ", error.code);
            };


        startTracking()
        var htracker;


        // THESE VARIABLES CONTROLL THE MAPPING OF THE HEAD MOVEMENT TO THE 3D MOTION
        // THE HIGHER v1 THE FASTER IS MOVES
        var v1 = 10;
        var v2 = v1 * 0.5;

        function startTracking() {
            htracker = new headtrackr.Tracker({
                ui: document.getElementById('game-stats'),
                //calcAngles: true,
                //debug: true
            });
            htracker.init(video, canvas);
            htracker.start();

            var v = 1;
            document.addEventListener('headtrackingEvent', function(event) {
                if (gameScene == 0 && tweening != true) {
                     spaceship.rotation.y = decInRad(360 / (Math.exp(Math.log((0.5 - event.px) * 0.7))));
                    //spaceship.rotation.z = decInRad(360 / (10-Math.exp(Math.log((0.5 - event.px) * v1))));
                    //spaceship.rotation.x = decInRad(360 / (10-Math.exp(Math.log((0.5 - event.py) * v1))));

                } else if (gameScene == 1) {
                    spaceship.position.setX(v2 - (Math.exp(Math.log((0.5 - event.px) * v1))));
                    spaceship.position.setY(v2 - (Math.exp(Math.log((0.5 - event.py) * v1))));
                }
                v++;
            }, false);


            document.addEventListener('facetrackingEvent', function(event) {

                v++;
            }, false);

            document.addEventListener("headtrackrStatus", function(event) {

                cameraStatus = event.status;


            }, false);
        }

        // ON RESIZE
        $(window).resize(onResize);

        // RESIZE FUNCTION
        function onResize(e) {
            // WINDOW SIZE
            width = window.innerWidth;
            height = window.innerHeight;

            // TEXT OVERLAY
            console.log("resized");
            $(statsDiv).width(width).height(height);

            /*$(statsDiv).css({
                "font-size": (width * 0.03) + 'px'
            });

            $*/

            // 3D MODELS
            camera.aspect = width / height;
            camera.updateProjectionMatrix();
            renderer.setSize(width, height);


        }

        // TIMER
        function updateTime(value) {
            $("#timer").text(value);

        }

        // ON KEYBOARD EVENT
        document.addEventListener('keydown', function(event) {
            if (event.keyIdentifier == "U+0050" && cameraStatus == "found")
                {
                    // POSITION SPACESHIP 
                    tweenSpaceship({x:0,y:0,z:0,rotX:0,rotY:270,rotZ:0});
                    /*spaceship.rotation.y = 270 * Math.PI / 180;
                    spaceship.rotation.x = 0;
                    spaceship.rotation.z = 0;*/

                    // CHANGE COLOR 

                    // AND CHANGE SCENE TO 1
                    window.setTimeout(function(){gameScene = 1;}, 1000);
                }
        }, false);

        // DECREES IN RADIANS
        function decInRad(value) {

            return value * Math.PI / 180
        }

    }) // end document ready